# AIS Restaurant
Semester school project to Advanced Information Systems course at Faculty of Information Technology, Brno University of Technology implementing information system for imaginary restaurant.

#### Authors:

- **Tomáš Líbal** - xlibal00
- **Pavel Eis** - xeispa00
- **Tomáš Chocholatý** - xchoch07
- **Lukáš Ondrák** - xondra49
- **Ondřej Novák** - xnovak2b

#### Technologies
- Angular, LESS, Bootstrap 4


# AISFE

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.14.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
