import {Injectable} from '@angular/core';
import {ConstantsRoutesModel} from '../../app/constants-routes.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MealsToPay, OrderItemIds} from '../../app/billing/billing.model';
import {HeaderSetterUtil} from '../../utils/HeaderSetterUtil';
import {ResponseModel} from '../../app/response.model';

@Injectable({
  providedIn: 'root'
})

export class BillingService {
  private readonly urlRoutesConstants: typeof ConstantsRoutesModel = ConstantsRoutesModel;

  constructor(private http: HttpClient) {

  }

  getMealsToPay(placeId: number): Observable<[MealsToPay]> {
    return this.http.get<any>(this.urlRoutesConstants.BILLING_ITEMS + placeId + this.urlRoutesConstants.PAYMENT,
      HeaderSetterUtil.setAuthorization());
  }

  sendMakePayment(orderItemIds: OrderItemIds, placeId: number): Observable<ResponseModel> {
    return this.http.post<any>(this.urlRoutesConstants.BILLING_ITEMS + placeId + this.urlRoutesConstants.PAYMENT,
      orderItemIds, HeaderSetterUtil.setAuthorization());
  }
}
