import {Component, OnInit} from '@angular/core';
import {ReservationService} from '../../../services/reservation/reservation.service';
import {NotifierDisplayerUtil} from '../../../utils/NotifierDisplayerUtil';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {AuthenticationConstants} from '../../authentication/authentication.constants';
import {Reservation} from '../../response.model';
import {DatePipe} from '@angular/common';
import {ReservationConstants} from './reservation.constants';
import {ReservationStateModel} from './reservation.model';
import {SweetalertInfoSetterUtil} from '../../../utils/SweetalertInfoSetterUtil';
import Swal from 'sweetalert2';
import 'sweetalert2/src/sweetalert2.scss';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';
import {ReservationsConstants} from './reservations.constants';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.less'],
  providers: [DatePipe]
})
export class ReservationsComponent implements OnInit {
  reservationsModel: Reservation[] = [];
  private readonly authConstants: typeof AuthenticationConstants = AuthenticationConstants;
  private readonly reservationsConstants: typeof ReservationsConstants = ReservationsConstants;
  private dataLoaded = false;

  constructor(private reservationService: ReservationService,
              private datePipe: DatePipe, private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.getAllReservations();
  }

  getAllReservations() {
    this.reservationService.getAllReservations().subscribe(
      res => {
        this.reservationsModel = res;
        this.dataLoaded = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  getDate(date: string) {
    return this.datePipe.transform(date, 'dd.MM.yyyy');
  }

  getTime(time: string) {
    return this.datePipe.transform(time, 'HH:mm');
  }

  getReservationType(placeType: string) {
    if (placeType === 'TABLE') {
      return ReservationConstants.PLACE_TABLE;
    } else if (placeType === 'LOUNGE') {
      return ReservationConstants.PLACE_LOUNGE;
    } else {
      console.error('Wrong reservation place type: ' + placeType);
    }
  }

  isFinished(reservationState: string) {
    return reservationState === 'FINISHED';
  }

  switchState(reservationToChange: Reservation) {
    const reservationState = reservationToChange.reservationState;
    let stateToSend: string;
    if (reservationState === 'APPROVED') {
      stateToSend = 'FINISHED';
    } else if (reservationState === 'FINISHED') {
      stateToSend = 'APPROVED';
    } else {
      return;
    }
    this.sendNewStateData(stateToSend, reservationToChange);
  }

  sendNewStateData(stateToSend: string, reservation: Reservation) {
    const reservationState: ReservationStateModel = {
      reservationState: stateToSend
    };
    if (stateToSend === 'DENIED') {
      Swal.fire(
        SweetalertInfoSetterUtil.getInfoForDeleteAlert('Opravdu chcete tuto rezervaci zrušit?')
      ).then((result) => {
        if (result.value) {
          this.reservationService.sendChangeOfState(reservationState, reservation.reservationId).subscribe(
            () => {
              this.disableAll(reservation.reservationId);
              NotifierDisplayerUtil.showNotification('success', 'Rezervace byla úspěšně zrušena');
              reservation.reservationState = stateToSend;
            },
            error => {
              ResponseErrorHandler.handleError(error);
            }
          );
        }
      });
    } else {
      this.reservationService.sendChangeOfState(reservationState, reservation.reservationId).subscribe(
        () => {
          reservation.reservationState = stateToSend;
          NotifierDisplayerUtil.showNotification('success', 'Status aktualizován');
        },
        error => {
          ResponseErrorHandler.handleError(error);
        }
      );
    }
  }

  getExpectedTime(expectedTime: number) {
    if (expectedTime === 0) {
      return 'Nevím';
    } else if (expectedTime === 1) {
      return '1\xa0hodina';
    } else if (expectedTime >= 2 && expectedTime <= 4) {
      return expectedTime + '\xa0hodiny';
    } else {
      return expectedTime + '\xa0hodin';
    }
  }

  disableAll(reservationIndex: number) {
    if (this.authService.hasUserSpecificRole(AuthenticationConstants.WAITER_NAME)) {
      const checkbox = document.getElementById('checkbox-' + reservationIndex) as HTMLInputElement;
      checkbox.disabled = true;
      checkbox.checked = false;
    }
    const deleteButton = document.getElementById('delete-button-' + reservationIndex) as HTMLInputElement;
    deleteButton.disabled = true;
    deleteButton.closest('div').classList.add('opacity');
  }

  isDisabled(reservationState: string) {
    return reservationState === 'DENIED';
  }

  isAccessible(role: string) {
    return this.authService.hasUserSpecificRole(role);
  }
}
