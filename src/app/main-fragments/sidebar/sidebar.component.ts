import {Component, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import {RouteInfo} from '../../../services/main-fragments/navigation-routes.service';
import {NavigationRoutesService} from '../../../services/main-fragments/navigation-routes.service';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  private sidebarItems: Array<RouteInfo>;
  private sidebarBackground: string;
  private innerWidth: any;
  @Output() hideSidebar = new EventEmitter();

  constructor(private routes: NavigationRoutesService, private authenticationService: AuthenticationService,
              private router: Router) {
  }

  ngOnInit() {
    this.sidebarItems = this.routes.routes;
    const routeInfo = this.sidebarItems.find(x => x.path === location.pathname);
    if (routeInfo === undefined) {
      this.sidebarBackground = this.sidebarItems[0].sidebarBackground;
    } else {
      this.sidebarBackground = routeInfo.sidebarBackground;
    }
    this.innerWidth = window.innerWidth;
  }

  updateSidebarBackground(imagePath: string) {
    this.sidebarBackground = imagePath;
  }

  isMobileMenu() {
    return this.innerWidth <= 991;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  logout() {
    if (localStorage.length === 0) {
      this.router.navigate(['/login']);
    } else {
      localStorage.clear();
      this.router.navigate(['/homepage']);
    }
  }

  hideSidebarOnClick() {
    if (this.isMobileMenu()) {
      this.hideSidebar.emit();
    }
  }

  redirectToHomepage() {
    this.authenticationService.getHomepage(this.router);
  }
}
