import {Component, HostListener} from '@angular/core';

@Component({
  selector: 'app-scroll-to-top',
  templateUrl: './scroll-to-top.component.html',
  styleUrls: ['./scroll-to-top.component.less']
})
export class ScrollToTopComponent {
  windowScrolled: boolean;
  topPosToStartShowing = 100;

  checkScroll() {
    const scrollPosition = document.getElementById('main-panel').scrollTop || 0;
    this.windowScrolled = scrollPosition >= this.topPosToStartShowing;
  }

  scrollToTop() {
    document.getElementById('main-panel').scroll({
      top: 0,
      behavior: 'smooth'
    });
  }
}
