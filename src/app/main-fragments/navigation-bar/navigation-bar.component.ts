import {Component, ElementRef, EventEmitter, OnInit, Output, Renderer2} from '@angular/core';
import {Location} from '@angular/common';
import {NavigationRoutesService} from '../../../services/main-fragments/navigation-routes.service';
import {RouteInfo} from '../../../services/main-fragments/navigation-routes.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.less']
})
export class NavigationBarComponent implements OnInit {
  private sidebarVisible: boolean;
  private toggleButton: any;
  private navbarItems: Array<RouteInfo>;
  private location: Location;
  @Output() updateBackgroundEvent = new EventEmitter();

  constructor(location: Location, private el: ElementRef,
              private renderer: Renderer2, private router: Router,
              private routes: NavigationRoutesService) {
    this.location = location;
    this.sidebarVisible = false;
  }

  ngOnInit() {
    this.navbarItems = this.routes.routes;
    const navbar: HTMLElement = this.el.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
  }

  logout() {
    if (localStorage.length === 0) {
      this.router.navigate(['/login']);
    } else {
      localStorage.clear();
      this.router.navigate(['/homepage']);
    }
  }

  sidebarOpen() {
    const body = document.getElementsByTagName('body')[0];
    this.renderer.addClass(this.toggleButton, 'toggled');
    this.renderer.addClass(body, 'nav-open');
    this.sidebarVisible = true;
  }

  sidebarClose() {
    const body = document.getElementsByTagName('body')[0];
    this.renderer.removeClass(this.toggleButton, 'toggled');
    this.renderer.removeClass(body, 'nav-open');
    this.sidebarVisible = false;
  }

  toggleSidebar() {
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }

  getTitle() {
    const title = this.location.prepareExternalUrl(this.location.path());
    const menuItems = '/menu-items/';
    if (title.startsWith('/')) {
      if (title.startsWith(menuItems)) {
          const sides = title.split(menuItems);
          if (!isNaN(Number(sides[1]))) {
            return 'Editace pokrmu';
          }
      }
      for (const item of this.navbarItems) {
        if (item.path === title) {
          return item.title;
        }
      }
    }
    return '';
  }

  updateSidebarBackground(imagePath: string) {
    this.updateBackgroundEvent.emit(imagePath);
  }
}
