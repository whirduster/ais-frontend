import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NotFoundComponent} from './not-found/not-found.component';
import {LoginComponent} from './unlogged-user-pages/login/login.component';
import {RegisterComponent} from './unlogged-user-pages/register/register.component';
import {ReservationsComponent} from './reservation/reservations/reservations.component';
import {CreateReservationComponent} from './unlogged-user-pages/create-reservation-unlogged/create-reservation.component';
import {NewEmployeeComponent} from './employees/new-employee/new-employee.component';
import {SalesComponent} from './sales/sales/sales.component';
import {ManageEmployeesComponent} from './employees/manage-employees/manage-employees.component';
import {EditProfileComponent} from './authentication/edit-profile/edit-profile.component';
import {LoggedUserOrEmployeeGuard} from '../guards/logged-or-employee-guard.service';
import {ManagerGuard} from '../guards/manager.guard';
import {ChefOrCookGuard} from '../guards/chef-or-cook-guard';
import {MenuItemsComponent} from './menu/menu-items/menu-items/menu-items.component';
import {UnloggedOrLoggedOrWaiterGuard} from '../guards/unlogged-or-logged-or-waiter.guard';
import {CategoryComponent} from './menu/category/category.component';
import {ChefGuard} from '../guards/chef.guard';
import {MenuMainComponent} from './menu/menu/menu-main.component';
import {NewMenuItemComponent} from './menu/menu-items/new-menu-item/new-menu-item.component';
import {EditMenuItemComponent} from './menu/menu-items/edit-menu-item/edit-menu-item.component';
import {ReservationSchemeComponent} from './reservation/create-reservation/restaurant-reservation-scheme/reservation-scheme.component';
import {WaiterGuard} from '../guards/waiter.guard';
import {OrderItemsComponent} from './order-items/order-items/order-items.component';
// tslint:disable-next-line:max-line-length
import {CreateReservationLoggedComponent} from './reservation/create-reservation/create-reservation-logged/create-reservation-logged.component';
import {RestaurantWaiterSchemeComponent} from './restaurant-scheme/restaurant-waiter-scheme/restaurant-waiter-scheme.component';
import {LoggedOrWaiterGuard} from '../guards/logged-or-waiter.guard';
import {BillingComponent} from './billing/billing.component';
import {WaiterOrderItemsComponent} from './order-items/waiter-order-items/waiter-order-items.component';
import {ChefOrCookOrWaiterGuard} from '../guards/chef-or-cook-or-waiter.guard';
import {EmployeeGuard} from '../guards/employee.guard';
import {HomepageUnloggedOrLoggedGuard} from '../guards/homepage-unlogged-or-logged-guard.service';
import {HomePageComponent} from './unlogged-user-pages/home-page/home-page.component';
import {UnloggedGuard} from '../guards/unlogged.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'homepage',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'homepage',
    component: HomePageComponent,
    canActivate: [HomepageUnloggedOrLoggedGuard]
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'account',
    component: EditProfileComponent,
    canActivate: [LoggedUserOrEmployeeGuard]
  },
  {
    path: 'menu',
    component: MenuMainComponent,
    canActivate: [EmployeeGuard]
  },
  {
    path: 'reservations',
    component: ReservationsComponent,
    canActivate: [UnloggedOrLoggedOrWaiterGuard]
  },
  {
    path: 'reservations/new',
    component: CreateReservationComponent,
    canActivate: [UnloggedGuard]
  },
  {
    path: 'reservation',
    component: CreateReservationLoggedComponent,
    canActivate: [LoggedUserOrEmployeeGuard]
  },
  {
    path: 'reservation/restaurant-scheme',
    component: ReservationSchemeComponent,
    canActivate: [LoggedOrWaiterGuard]
  },
  {
    path: 'employees/new',
    component: NewEmployeeComponent,
    canActivate: [ManagerGuard]
  },
  {
    path: 'employees/edit',
    component: ManageEmployeesComponent,
    canActivate: [ManagerGuard]
  },
  {
    path: 'sales',
    component: SalesComponent,
    canActivate: [ManagerGuard]
  },
  {
    path: 'menu-items',
    component: MenuItemsComponent,
    canActivate: [ChefOrCookGuard]
  },
  {
    path: 'menu-items/new',
    component: NewMenuItemComponent,
    canActivate: [ChefGuard]
  },
  {
    path: 'menu-items/:id',
    component: EditMenuItemComponent,
    canActivate: [ChefGuard]
  },
  {
    path: 'menu/categories',
    component: CategoryComponent,
    canActivate: [ChefGuard]
  },
  {
    path: 'restaurant-scheme',
    component: RestaurantWaiterSchemeComponent,
    canActivate: [WaiterGuard]
  },
  {
    path: 'order-items',
    component: OrderItemsComponent,
    canActivate: [ChefOrCookOrWaiterGuard]
  },
  {
    path: 'payment/:id',
    component: BillingComponent,
    canActivate: [WaiterGuard]
  },
  {
    path: 'order/:id',
    component: WaiterOrderItemsComponent,
    canActivate: [WaiterGuard]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
