import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BillingConstants} from './billing.constants';
import {MealsToPay, OrderItemIds, ReceiptsItem} from './billing.model';
import {BillingService} from '../../services/billing/billing.service';
import {ResponseErrorHandler} from '../../utils/ResponseErrorHandler';
import {ArrayItemManipulatorUtil} from '../../utils/ArrayItemManipulatorUtil';
import {NotifierDisplayerUtil} from '../../utils/NotifierDisplayerUtil';
import Swal from "sweetalert2";

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: [
    './billing.component.less',
    '../menu/menu.less'
  ]
})
export class BillingComponent implements OnInit {
  private billingConstants: typeof BillingConstants = BillingConstants;
  private id;
  private totalPrice = 0;
  private dataLoaded = false;

  mealsFromOrder: MealsToPay[] = [];
  mealsToPay: MealsToPay[] = [];
  orderItemIds: OrderItemIds = {
    orderItemIds: []
  };

  constructor(private activatedRoute: ActivatedRoute, private billingService: BillingService) {

  }

  ngOnInit(): void {
    this.id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.getMealsToPay(this.id);
  }

  getMealsToPay(placeId: number) {
    this.billingService.getMealsToPay(placeId).subscribe(
      res => {
        this.mealsFromOrder = res;
        this.mealsFromOrder = this.mealsFromOrder.filter(x => x.receiptsItems.length > 0);
        this.dataLoaded = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  moveItemToPayOrBack(receiptsItem: ReceiptsItem, mealFromOrder: MealsToPay, fromList: Array<MealsToPay>, toList: Array<MealsToPay>,
                      check: boolean) {
    if (check && receiptsItem.state !== 'RELEASED') {
      return;
    }

    const fromOrder = fromList.find(s => s.categoryName === mealFromOrder.categoryName);
    const toBill = toList.find(s => s.categoryName === mealFromOrder.categoryName);

    if (toBill === undefined) {
      toList.push({categoryName: mealFromOrder.categoryName, receiptsItems: [receiptsItem]});
    } else {
      toBill.receiptsItems.push(receiptsItem);
    }

    this.getTotalPriceToPay(receiptsItem, check);
    ArrayItemManipulatorUtil.deleteItemFromArray(receiptsItem, fromOrder.receiptsItems);
    if (fromOrder.receiptsItems.length < 1) {
      ArrayItemManipulatorUtil.deleteItemFromArray(fromOrder, fromList);
    }
  }

  getTotalPriceToPay(receiptsItem: ReceiptsItem, check: boolean) {
    if (check) {
     this.totalPrice += receiptsItem.price;
    } else {
      this.totalPrice -= receiptsItem.price;
    }
  }

  makePayment() {
    (document.getElementById('submitButton') as HTMLButtonElement).disabled = true;
    for (const x of this.mealsToPay) {
      for (const y of x.receiptsItems) {
        this.orderItemIds.orderItemIds.push(y.orderItemId);
      }
    }
    this.billingService.sendMakePayment(this.orderItemIds, this.id).subscribe(
      () => {
        this.mealsFromOrder = this.mealsFromOrder.filter(x => x.receiptsItems.length > 0);
        this.mealsToPay = [];
        this.totalPrice = 0;
        NotifierDisplayerUtil.showNotification('success', 'Platba byla úspěšně provedena.');
      },
      error => {
        ResponseErrorHandler.handleError(error);
        (document.getElementById('submitButton') as HTMLButtonElement).disabled = false;
      }
    );
  }

  getOrderState(state: string) {
    if (state !== 'RELEASED') {
      return 'Nevydaná objednávka';
    } else {
      return 'Lze zaplatit';
    }
  }
}
