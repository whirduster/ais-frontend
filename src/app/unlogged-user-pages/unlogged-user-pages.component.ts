import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'app-unlogged-user-pages',
  templateUrl: './unlogged-user-pages.component.html',
  styleUrls: ['./unlogged-user-pages.component.less']
})
export class UnloggedUserPagesComponent implements OnInit {
  isHomePage = new RegExp('(^/homepage$|^/homepage[\/?#].*)');
  isLoginPage = new RegExp('(^/login$|^/login[\/?#].*)');
  isRegisterPage = new RegExp('(^/register$|^/register[\/?#].*)');
  isCreateReservationPage = new RegExp('(^/reservations/new$|^/reservations/new[\/?#].*)');

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

}
