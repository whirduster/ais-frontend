import {Component, ElementRef, HostListener, OnInit, Renderer2} from '@angular/core';
import {CreateReservationConstants} from '../../reservation/create-reservation/create-reservation.constants';
import {CreateReservationModel} from '../../reservation/create-reservation/create-reservation.model';
import {Router} from '@angular/router';
import {ReservationService} from '../../../services/reservation/reservation.service';
import {DatePipe} from '@angular/common';
import {SweetalertInfoSetterUtil} from '../../../utils/SweetalertInfoSetterUtil';
import {NotifierDisplayerUtil} from '../../../utils/NotifierDisplayerUtil';
import {ResponseErrorHandler} from '../../../utils/ResponseErrorHandler';
import {CreateReservation} from './create-reservation';

@Component({
  selector: 'app-create-reservation',
  templateUrl: './create-reservation.component.html',
  styleUrls: [
    './create-reservation.component.less',
    '../unlogged-user-pages.component.less'
  ],
  providers: [DatePipe]
})
export class CreateReservationComponent implements OnInit {

  constructor(private reservationService: ReservationService, private router: Router, private datePipe: DatePipe,
              private renderer: Renderer2, private el: ElementRef) {
  }
  private readonly createReservationConstants: typeof CreateReservationConstants = CreateReservationConstants;
  private toggleButton: any;
  private sidebarVisible: boolean;
  private windowScrolled: boolean;

  reservation: CreateReservationModel = {
    hostInfo: {
      email: '',
      name: '',
      surname: '',
    },
    reservationInfo: {
      date: '',
      time: '',
      expectedLength: 0,
      hostsCount: 0,
    }
  };

  codeForDelete: CreateReservation = {
    code: ''
  };

  ngOnInit() {
    const navbar: HTMLElement = this.el.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
  }

  sendNewReservationData() {
    (document.getElementById('submitButton') as HTMLButtonElement).disabled = true;
    SweetalertInfoSetterUtil.waitForReservationCreation('load', null);
    this.reservationService.sendNewReservationUnloggedUserData(this.reservation).subscribe(
      () => {
        SweetalertInfoSetterUtil.waitForReservationCreation(true, 'Rezervace byla úspěšně vytvořena.');
        this.router.navigate(['/homepage']);
      },
      error => {
        SweetalertInfoSetterUtil.waitForReservationCreation(false, error);
        (document.getElementById('submitButton') as HTMLButtonElement).disabled = false;
      }
    );
  }

  isDateRight() {
    return this.reservationService.isDateRight(this.reservation.reservationInfo.date, this.datePipe);
  }

  isHostsCountRight() {
    return this.reservationService.isHostsCountRight(this.reservation.reservationInfo.hostsCount);
  }

  isTimeRight() {
    return this.reservationService.isTimeRight(this.reservation.reservationInfo.date, this.reservation.reservationInfo.time, this.datePipe);
  }

  isTimeInOpeningHours() {
    return this.reservationService.isTimeInOpeningHours(this.reservation.reservationInfo.time);
  }

  isTimeBeforeClosing() {
    return !this.reservationService.isTimeBeforeClosing(this.reservation.reservationInfo.time);
  }


  sidebarOpen() {
    const nav = document.getElementsByTagName('nav')[0];
    this.renderer.addClass(this.toggleButton, 'toggled');
    this.renderer.addClass(nav, 'nav-open');
    this.sidebarVisible = true;
  }

  sidebarClose() {
    const nav = document.getElementsByTagName('nav')[0];
    this.renderer.removeClass(this.toggleButton, 'toggled');
    this.renderer.removeClass(nav, 'nav-open');
    this.sidebarVisible = false;
  }

  toggleSidebar() {
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    const scrollPosition = document.documentElement.scrollTop || 0;
    this.windowScrolled = scrollPosition >= 20;
  }

  isTablet() {
    return window.innerWidth <= 991;
  }

  @HostListener('window:resize', ['$event'])
  isMobile() {
    return window.innerWidth <= 515;
  }

  sendCodeForDeleteReservation() {
    this.reservationService.sendCodeForDeleteReservation(this.codeForDelete).subscribe(
      () => {
        NotifierDisplayerUtil.showNotification('success', 'Rezervace byla úspěšně zrušena.');
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }
}
