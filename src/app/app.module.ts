import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { SidebarModule } from './main-fragments/sidebar/sidebar.module';
import { HighchartsChartModule } from 'highcharts-angular';

import { AppComponent } from './app/app.component';
import { NavigationBarComponent } from './main-fragments/navigation-bar/navigation-bar.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FooterComponent } from './main-fragments/footer/footer.component';
import { LoginComponent } from './unlogged-user-pages/login/login.component';
import { RegisterComponent } from './unlogged-user-pages/register/register.component';
import { ChefMenuComponent } from './menu/menu/chef-menu/chef-menu.component';
import { ReservationsComponent } from './reservation/reservations/reservations.component';
import { CreateReservationComponent } from './unlogged-user-pages/create-reservation-unlogged/create-reservation.component';
import { HomePageComponent } from './unlogged-user-pages/home-page/home-page.component';
import { ManageEmployeesComponent } from './employees/manage-employees/manage-employees.component';
import { NewEmployeeComponent } from './employees/new-employee/new-employee.component';
import { SalesComponent } from './sales/sales/sales.component';

import { EditProfileComponent } from './authentication/edit-profile/edit-profile.component';
import { MenuItemsComponent } from './menu/menu-items/menu-items/menu-items.component';
import { CategoryComponent } from './menu/category/category.component';
import { MenuMainComponent } from './menu/menu/menu-main.component';
import { NormalMenuComponent } from './menu/menu/normal-menu/normal-menu.component';
import { NewMenuItemComponent } from './menu/menu-items/new-menu-item/new-menu-item.component';
import { EditMenuItemComponent } from './menu/menu-items/edit-menu-item/edit-menu-item.component';
import { ReservationSchemeComponent } from './reservation/create-reservation/restaurant-reservation-scheme/reservation-scheme.component';
import { OrderItemsComponent } from './order-items/order-items/order-items.component';
import { WaiterOrderItemsComponent } from './order-items/waiter-order-items/waiter-order-items.component';
// tslint:disable-next-line:max-line-length
import { CreateReservationLoggedComponent } from './reservation/create-reservation/create-reservation-logged/create-reservation-logged.component';
import { RestaurantWaiterSchemeComponent } from './restaurant-scheme/restaurant-waiter-scheme/restaurant-waiter-scheme.component';
import { BillingComponent } from './billing/billing.component';
import { ScrollToTopComponent } from './main-fragments/scroll-to-top/scroll-to-top.component';
import { UnloggedUserPagesComponent } from './unlogged-user-pages/unlogged-user-pages.component';

import {NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    NotFoundComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    ChefMenuComponent,
    ReservationsComponent,
    CreateReservationComponent,
    HomePageComponent,
    ManageEmployeesComponent,
    NewEmployeeComponent,
    SalesComponent,
    EditProfileComponent,
    MenuItemsComponent,
    CategoryComponent,
    MenuMainComponent,
    NormalMenuComponent,
    NewMenuItemComponent,
    EditMenuItemComponent,
    ReservationSchemeComponent,
    OrderItemsComponent,
    WaiterOrderItemsComponent,
    ReservationSchemeComponent,
    CreateReservationLoggedComponent,
    RestaurantWaiterSchemeComponent,
    BillingComponent,
    ScrollToTopComponent,
    UnloggedUserPagesComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    SidebarModule,
    AppRoutingModule,
    HighchartsChartModule,
    NgbCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
