import {HostInfo} from './reservation/create-reservation/create-reservation.model';
import {CategoryOfMenuItems, CategoryOfOrderItems} from './order-items/order-items.model';

export interface ResponseModel {
  message: string;
}

export interface TokenResponseModel extends ResponseModel {
  token: string;
}

export interface LoginResponseModel {
  roles: Array<string>;
  token: string;
}

export class Employee {
  employeeInfo: EmployeeInfo;
  roles: Array<string>;
}

export interface EmployeeInfo {
  email: string;
  name: string;
  surname: string;
}

export class Reservation {
  reservationId: number;
  placeType: string;
  placeId: number;
  expectedTime: number;
  date: string;
  reservationState: string;
  hostInfo: HostInfo;
}

export class AllCategoriesResponse {
  categoryId: number;
  name: string;
}


export interface GetFreeAndOccupiedTablesResponse {
  freeTables: number[];
  occupiedTables: number[];
}

export interface OrderItemsOfPlaceResponseModel {
  'categoriesOrderItems': [
    CategoryOfOrderItems
  ];
  'categoriesMenuItems': [
    CategoryOfMenuItems
  ];
}

export interface ResponseModelWithId {
  orderItemId: number;
}
