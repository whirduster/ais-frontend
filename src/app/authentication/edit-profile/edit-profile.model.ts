export interface EditProfileModel {
  email: string;
  name: string;
  surname: string;
  oldPassword: string;
  password: string;
}
