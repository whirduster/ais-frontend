import {AfterViewChecked, Component, HostListener, OnInit} from '@angular/core';
import {ArrayItemManipulatorUtil} from '../../../../utils/ArrayItemManipulatorUtil';
import {MenuService} from '../../../../services/menu/menu.service';
import {ResponseErrorHandler} from '../../../../utils/ResponseErrorHandler';
import {Category, ChefMenuModel} from './chef-menu.model';
import {AuthenticationService} from '../../../../services/authentication/authentication.service';
import {MenuConstants} from '../../menu.constants';
import {NotifierDisplayerUtil} from '../../../../utils/NotifierDisplayerUtil';
import {ActionButtonUtil} from '../../../../utils/ActionButtonUtil';
import Swal from 'sweetalert2';
import 'sweetalert2/src/sweetalert2.scss';
import {SweetalertInfoSetterUtil} from '../../../../utils/SweetalertInfoSetterUtil';

@Component({
  selector: 'app-chef-menu',
  templateUrl: './chef-menu.component.html',
  styleUrls: [
    './chef-menu.component.less',
    '../../menu.less'
  ]
})
export class ChefMenuComponent implements OnInit, AfterViewChecked {

  private menuConstants: typeof MenuConstants = MenuConstants;
  private dataLoaded = false;

  categoriesInMenu: [Category];
  categoriesNotInMenu: [Category];

  constructor(private authService: AuthenticationService,
              private menuService: MenuService) {
  }

  ngOnInit() {
    this.getMenu();
  }

  ngAfterViewChecked() {
    ActionButtonUtil.setStyle();
  }

  getMenu() {
    this.menuService.getMenuInCategories().subscribe(
      res => {
        this.categoriesInMenu = res.categoriesInMenu;
        this.categoriesNotInMenu = res.categories;
        this.sortListsAplhabetically();
        this.dataLoaded = true;
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }


  sendEditMenuCategories() {
    const model: ChefMenuModel = {
      categoriesInMenu: this.categoriesInMenu,
      categories: this.categoriesNotInMenu
    };
    this.menuService.editMenuCategories(model).subscribe(
      res => {
        NotifierDisplayerUtil.showNotification('success', 'Menu bylo úspěšně upraveno.');
      },
      error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  restartCategories() {
    this.getMenu();
  }

  moveItem(menuItem, categoryToChange: Category, fromList: Array<Category>, toList: Array<Category>) {
    const fromCategory = fromList.find(category => category.categoryName === categoryToChange.categoryName);
    const toCategory = toList.find(category => category.categoryName === categoryToChange.categoryName);

    if (toCategory === undefined) {
      toList.push({categoryName: categoryToChange.categoryName, menuItems: [menuItem]});
    } else {
      toCategory.menuItems.push(menuItem);
    }

    this.sortListsAplhabetically();

    ArrayItemManipulatorUtil.deleteItemFromArray(menuItem, fromCategory.menuItems);
    if (fromCategory.menuItems.length < 1) {
      ArrayItemManipulatorUtil.deleteItemFromArray(fromCategory, fromList);
    }
  }

  private sortListsAplhabetically() {
    this.categoriesInMenu.sort((
      (a, b) =>
        a.categoryName.localeCompare(b.categoryName)));

    this.categoriesNotInMenu.sort((
      (a, b) =>
        a.categoryName.localeCompare(b.categoryName)));

    for (const category of this.categoriesInMenu) {
      category.menuItems.sort((
        (a, b) =>
          a.name.localeCompare(b.name)));
    }

    for (const category of this.categoriesNotInMenu) {
      category.menuItems.sort((
        (a, b) =>
          a.name.localeCompare(b.name)));
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    ActionButtonUtil.setStyle();
  }

  showPopupMenuItemInfo(menuItemId: number) {
    this.menuService.getMenuItem(menuItemId).subscribe(
      res => {
        Swal.fire(SweetalertInfoSetterUtil.getMenuItemFullInfoAlert(res));
      }, error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }
}
