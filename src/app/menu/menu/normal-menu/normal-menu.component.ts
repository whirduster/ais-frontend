import { Component, OnInit } from '@angular/core';
import {MenuConstants} from '../../menu.constants';
import {Category} from '../chef-menu/chef-menu.model';
import {MenuService} from '../../../../services/menu/menu.service';
import {ResponseErrorHandler} from '../../../../utils/ResponseErrorHandler';
import {Router} from '@angular/router';

@Component({
  selector: 'app-normal-menu',
  templateUrl: './normal-menu.component.html',
  styleUrls: [
    './normal-menu.component.less',
    '../../menu.less'
  ]
})
export class NormalMenuComponent implements OnInit {

  private menuConstants: typeof MenuConstants = MenuConstants;
  private categories: Category[] = [];
  private dataLoaded = false;

  constructor(
    private menuService: MenuService,
    public router: Router
  ) {
  }
  regexp = new RegExp('(^/homepage$|^/homepage[\\/?#].*)');

  ngOnInit() {
    this.getAllMenuItemsInMenu();
  }

  getAllMenuItemsInMenu() {
    this.menuService.getAllMenuItemsInMenu().subscribe(
      res => {
        this.categories = this.deleteEmptyCategories(res);
        this.dataLoaded = true;
      }, error => {
        ResponseErrorHandler.handleError(error);
      }
    );
  }

  private deleteEmptyCategories(categories: Category[]): Category[] {
    return categories.filter(x => x.menuItems.length > 0);
  }
}
