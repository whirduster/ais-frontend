import {AuthenticationConstants} from '../authentication/authentication.constants';

export class EmployeesConstants {
  public static readonly CREATE_EMPLOYEE_ACCOUNT = 'Vytvoření nového zaměstnance';
  public static readonly CREATE_EMPLOYEE_ACCOUNT_BUTTON = 'Vytvořit zaměstnance';
  public static readonly SELECT_CHECKBOX = 'Přiřaďte alespoň jednu roli';
  public static readonly ROLES_ARRAY = [
      { id: 1, name: AuthenticationConstants.WAITER_NAME,  czechName: 'Číšník',    defaultValue: true,    enabled: true    },
      { id: 2, name: AuthenticationConstants.COOK_NAME,    czechName: 'Kuchař',    defaultValue: false,   enabled: true   },
      { id: 3, name: AuthenticationConstants.CHEF_NAME,    czechName: 'Šéfkuchař', defaultValue: false,   enabled: true   },
      { id: 4, name: AuthenticationConstants.MANAGER_NAME, czechName: 'Manažer',   defaultValue: false,   enabled: false   }
  ];

  public static readonly ADMIN_EMAIL = 'admin@restaurant.cz';
  public static readonly FILL_VALUE_TO_INPUT = 'Hodnota nebyla uložena, prosím vyplňte pole!';
  public static readonly NOT_CORRECT_EMAIL_FORMAT = 'Zadaná hodnota nemá správný formát emailu!';
  public static readonly CHANGED_SUCCESSFULLY = 'Hodnota byla úspěšně změněna.';
  public static readonly CHANGE_COULD_NOT_BE_DONE = 'Nelze zrušit poslední roli!';
  public static readonly NO_EMPLOYEES_IN_DB = 'V databázi není žádný zaměstnanec. Pro možnost úpravy nejprve nějakého přidejte.';
  public static readonly NEW_EMPLOYEE = 'Přidat nového zaměstnance';
}
