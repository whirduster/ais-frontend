import {EmployeeInfo} from '../../response.model';

export interface NewEmployeeModel {
  employeeInfo: EmployeeInfo;
  roles: Role[];
}

class Role {
  name: string;
  hasRole: boolean;
}
