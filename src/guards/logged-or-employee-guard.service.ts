import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedUserOrEmployeeGuard implements CanActivate {

  constructor(private authService: AuthenticationService, private router: Router) {
  }

  canActivate(): boolean {
    if (this.authService.isUserNotLoggedIn()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

}
